<?php

/**
 * Settings for payment method
 */
function commerce_sberbank_payment_settings_form($form, $form_state) {
  module_load_include('inc', 'commerce_sberbank_payment', 'commerce_sberbank_payment.constants');

  $form[SBERBANK_MERCHANT_USERNAME] = array(
    '#type' => 'textfield',
    '#title' => t('Sberbank merchant name'),
    '#default_value' => variable_get(SBERBANK_MERCHANT_USERNAME, 'name-api'),
    '#description' => t('Set API user name for Sberbank'),
  );
  $form[SBERBANK_MERCHANT_PASSWORD] = array(
    '#type' => 'textfield',
    '#title' => t('Sberbank merchant password'),
    '#default_value' => variable_get(SBERBANK_MERCHANT_PASSWORD, 'pass-api'),
    '#description' => t('Set API user password for Sberbank'),
  );
  return system_settings_form($form);
}

/**
 * Validation handler for configuration form.
 */
function commerce_sberbank_payment_settings_form_validate($form, &$form_state) {
  if ($form_state['values'] || empty($form_state['values'])) {
    if (empty($form_state['values'][SBERBANK_MERCHANT_USERNAME])) {
      form_set_error(SBERBANK_MERCHANT_USERNAME, t("Empty Sberbank merchant name"));
    }
    if (empty($form_state['values'][SBERBANK_MERCHANT_PASSWORD])) {
      form_set_error(SBERBANK_MERCHANT_PASSWORD, t("Empty Sberbank merchant password"));
    }
  }
}
