<?php
/**
 * Pages callbacks and validations.
 */
function sg_commerce_sberbank_pay() {
  global $base_url;
  module_load_include('inc', 'commerce_sberbank_payment', 'commerce_sberbank_payment.constants');
  module_load_include('inc', 'commerce_sberbank_payment', 'commerce_sberbank_payment.common');

  if (!isset($_POST)) {
    return drupal_not_found();
  }

  $nonprofit_keys = array(
    'form_build_id',
    'form_token',
    'form_id',
    'op'
  );

  $data = array();
  $responce = array();

  // parse form data
  foreach ($_POST as $key => $value) {
    if (!in_array($key, $nonprofit_keys)) {
      $data[$key] = $value;
    }
  }

  $responce = _commerce_sberbank_request_post(SBERBANK_REGISTER_URL, $data);
  if (!property_exists($responce, SBERBANK_RESPONCE_ERROR_CODE)) {
    $iframe = '<iframe name="commerce_sberbank_payment" id="commerce_sberbank_payment" src="' . $responce->{SBERBANK_RESPONCE_FORM_URL} . '" style="width: 100%; height: 700px; border: 0 none;" scrolling="no" frameborder="0"></iframe>';
    return $iframe;
    exit();
  }
  else {
    $message =_commerce_sberbank_request_errors($responce->{SBERBANK_RESPONCE_ERROR_CODE});
    return $message;
    watchdog("sg_sberbank", $message);
  }
}

/**
 * Transaction completion callback.
 */
function sg_commerce_sberbank_complete($order_id) {
  $message = t("Thank You, Order @order_id is now complete!", array('@order_id' => $order_id));
  return $message;
}

/**
 * Transaction completion callback.
 */
function sg_commerce_sberbank_failed($order_id, $error_code) {
  $error = _commerce_sberbank_request_errors($error_code);
  $message = t("Sorry, Transaction for @order_id is failed! See: @error ", array('@order_id' => $order_id, '@error' => $error));
  return $message;
}
