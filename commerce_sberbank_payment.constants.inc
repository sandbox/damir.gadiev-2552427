<?php
/**
 * @file
 * Constants holder
 */
// Registration
define('SBERBANK_REGISTER_URL', 'https://3dsec.sberbank.ru/payment/rest/register.do');
// Preauthorized registration
define('SBERBANK_REGISTER_PREAUTH_URL', 'https://3dsec.sberbank.ru/payment/rest/registerPreAuth.do');
// Completion of order payment url
define('SBERBANK_DEPOSIT_URL', 'https://3dsec.sberbank.ru/payment/rest/deposit.do');
// Cancellation of order payment
define('SBERBANK_REVERSE_URL', 'https://3dsec.sberbank.ru/payment/rest/reverse.do');
// Refund order payment
define('SBERBANK_REFUND_URL', 'https://3dsec.sberbank.ru/payment/rest/refund.do');
// Get order status
define('SBERBANK_ORDER_STATUS_URL', 'https://3dsec.sberbank.ru/payment/rest/getOrderStatus.do');
// Get extended order status
define('SERBANK_EXTENDED_ORDER_STATUS_URL', 'https://3dsec.sberbank.ru/payment/rest/getOrderStatusExtended.do');
// Verify 3ds involvement
define('SBERBANK_VERIFY_ENROLLMENT_URL', 'https://3dsec.sberbank.ru/payment/rest/verifyEnrollment.do');
// Query payment of order by bindings
define('SBERBANK_PAYMENT_ORDER_BINDING_URL', 'https://3dsec.sberbank.ru/payment/rest/paymentOrderBinding.do');
// Binding deactivation query
define('SBERBANK_UNBIND_CARD_URL', 'https://3dsec.sberbank.ru/payment/rest/unBindCard.do');
// Binding activation query
define('SBERBANK_BIND_CARD_URL', 'https://3dsec.sberbank.ru/payment/rest/bindCard.do');
// Request for change of the term of the binding
define('SBERBANK_EXTEND_BINDING_URL', 'https://3dsec.sberbank.ru/payment/rest/extendBinding.do');
// Get bindings for merchant
define('SBERBANK_MERCHANT_GET_BINDINGS_URL', 'https://3dsec.sberbank.ru/payment/rest/getBindings.do');
// Get statistics on payments by period
define('SBERBANK_GET_LAST_ORDERS_FOR_MERCHANT_URL', 'https://3dsec.sberbank.ru/payment/rest/getLastOrdersForMerchants.do');


/**
 * Register request definitions.
 */
define('SBERBANK_MERCHANT_USERNAME', 'userName');
define('SBERBANK_MERCHANT_PASSWORD', 'password');
define('SBERBANK_STORE_ORDER_ID', 'orderNumber');
define('SBERBANK_STORE_ORDER_AMOUNT', 'amount');
define('SBERBANK_STORE_ORDER_CURRENCY', 'currency');
define('SBERBANK_RETURN_URL', 'returnUrl');
define('SBERBANK_FAIL_URL', 'failUrl');
define('SBERBANK_ORDER_DESCRIPTION', 'description');
define('SBERBANK_ORDER_LANGUAGE', 'language');
define('SBERBANK_ORDER_CLIENT_ID', 'clientId');
define('SBERBANK_ORDER_JSON_PARAMS', 'jsonParams');
define('SBERBANK_ORDER_SESSION_TIMEOUT', 'sessionTimeoutSecs');
define('SBERBANK_ORDER_SESSION_EXPIRATION_DATE', 'expirationDate');
define('SBERBANK_ORDER_BINDING_ID', 'bindingId');

/**
 * Register responce definintions.
 */
define('SBERBANK_RESPONCE_ORDER_ID', 'orderID');
define('SBERBANK_RESPONCE_FORM_URL', 'formUrl');
define('SBERBANK_RESPONCE_ERROR_CODE', 'errorCode');
define('SBERBANK_RESPONCE_ERROR_MESSAGE', 'errorMessage');
