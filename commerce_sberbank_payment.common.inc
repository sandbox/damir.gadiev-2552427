<?php
/**
 * @file
 * Common utils.
 */
/**
 * Send a POST request to Sberbank and return the response as an array.
 *
 * @param string $url
 *  The url to POST to.
 * @param array $data
 *  The data to post.
 *
 * @return array
 *  The response from Sage Pay.
 */
function _commerce_sberbank_request_post($url, $data) {
  $output = array();
  $curl_session = curl_init();

  curl_setopt($curl_session, CURLOPT_URL, $url);
  curl_setopt($curl_session, CURLOPT_HEADER, 0);
  curl_setopt($curl_session, CURLOPT_POST, 1);
  curl_setopt($curl_session, CURLOPT_POSTFIELDS, $data);
  curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_session, CURLOPT_TIMEOUT, 30);


  $rawresponse = curl_exec($curl_session);
  $output = json_decode($rawresponse);

  if (curl_error($curl_session)) {
    $output['Status'] = "FAIL";
    $output['StatusDetail'] = curl_error($curl_session);
  }

  curl_close($curl_session);
  return $output;
}

/**
 * Checks error from registration
 */
function _commerce_sberbank_request_errors($error_code) {
  if ($error_code) {
    $error_message = '';
    switch ($error_code) {
      case 0:
        $error_message = t("Request processed without errors", array(), array('context' => 'SG:Sberbank'));
        break;
      case 1:
        $error_message = t("Order is already registered with current number", array(), array('context' => 'SG:Sberbank'));
        break;
      case 3:
        $error_message = t("Undefined currency", array(), array('context' => 'SG:Sberbank'));
        break;
      case 4:
        $error_message = t("Required request parameter is missing", array(), array('context' => 'SG:Sberbank'));
        break;
      case 6:
        $error_message = t("Wrong parameter requested", array(), array('context' => 'SG:Sberbank'));
        break;
      case 7:
        $error_message = t("System error", array(), array('context' => 'SG:Sberbank'));
        break;
    }
    return $error_message;
  }
  else {
    return FALSE;
  }
}

/**
 * Helper to get ISO_4217
 */
function _commerce_sberbank_iso4217_format($code) {
  if ($code) {
    switch ($code) {
      case 'RUB':
        return 643;
        break;
      default:
        return 643;
    }
  }
  return FALSE;
}